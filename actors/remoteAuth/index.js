import { readFileSync } from 'fs';

import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import gql from 'graphql-tag';

import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import fetch from 'node-fetch';
import AssertionTokenType from '../../../../token-types/AssertionTokenType';
import createClientKey from '../../../../token-types/createClientKey';

import config from '../../../../config/server_config';
import db from '../../../../db';

const { query, dispatch, spawnStateless } = require('nact');

const tokenGraphql = readFileSync(`${__dirname}/graphql/token.graphql`, 'utf8');
const introspectGraphql = readFileSync(`${__dirname}/graphql/introspect.graphql`, 'utf8');
const userInfoGraphql = readFileSync(`${__dirname}/graphql/userInfo.graphql`, 'utf8');
const getCurrentClientGraphql = readFileSync(`${__dirname}/graphql/getCurrentClient.graphql`, 'utf8');

export default (parent) => spawnStateless(
  parent,
  async (msg, ctx) => {
    try {
      const client = await db.client.findOne({ application_type: config.application_type });
      const assertionTokenKey = await createClientKey(client);
      const assertionToken = new AssertionTokenType('', assertionTokenKey);
      await assertionToken.createSignToken(client, client);
      const assertion_token = assertionToken.token;

      const collectionItemActor = ctx.children.get('item');
      const auth_server = (await db.client.find({ application_type: 'auth_server' }))[0];
      const api_uri = `${auth_server.uri}/graphql`;

      const httpLinkNone = createHttpLink({
        uri: api_uri,
        fetch,
        headers: {},
      });
      let apolloClient = new ApolloClient({
        link: ApolloLink.from([
          httpLinkNone,
        ]),
        cache: new InMemoryCache(),
      });
      let variables = {
        input: {
          assertion: assertion_token,
          grant_type: 'jwt-bearer',
        },
      };
      const graphqlMutation = gql`
                ${tokenGraphql}
            `;
      let graphqlResult = await apolloClient.mutate({
        mutation: graphqlMutation,
        variables,
      });
      const { access_token } = graphqlResult.data.token;

      const httpLinkAuth = createHttpLink({
        uri: api_uri,
        fetch,
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      });
      apolloClient = new ApolloClient({
        link: ApolloLink.from([
          httpLinkAuth,
        ]),
        cache: new InMemoryCache(),
      });

      variables = {
        token: msg.token,
        token_type_hint: 'access_token',
      };
      let graphqlQuery = gql`
                ${introspectGraphql}
            `;
      graphqlResult = await apolloClient.query({
        query: graphqlQuery,
        variables,
      });
      const { introspect } = graphqlResult.data;
      const httpLinkToken = createHttpLink({
        uri: api_uri,
        fetch,
        headers: {
          Authorization: `Bearer ${msg.token}`,
        },
      });
      apolloClient = new ApolloClient({
        link: ApolloLink.from([
          httpLinkToken,
        ]),
        cache: new InMemoryCache(),
      });
      graphqlQuery = gql`
                ${userInfoGraphql}
            `;
      const tokenUser = await apolloClient.query({
        query: graphqlQuery,
      });
      graphqlQuery = gql`
                ${getCurrentClientGraphql}
            `;
      const tokenClient = await apolloClient.query({
        query: graphqlQuery,
      });

      dispatch(ctx.sender, {
        status: 'ok', data: introspect, user: tokenUser.data.userInfo, client: tokenClient.data.getCurrentClient,
      }, ctx.self);
    } catch (e) {
      console.log(e);
      dispatch(ctx.sender, { status: 'error', error: e.message }, ctx.self);
    }
  },
  'remote_auth',

);
